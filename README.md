# BankToQIF #

Chrome extension that creates [QIF](http://en.wikipedia.org/wiki/Quicken_Interchange_Format) report from [Privat24](https://www.privat24.ua/) or [Aval Corporate](https://vipiska.aval.ua/) transactions report

* Version: **Very early Alpha**

### How do I get set up? ###

* Deployment instructions: clone the repository, goto Chrome->Settings->Extensions->Load unpacked extension...->Select directory with project->Enjoy :)

### Who do I talk to? ###

* This is a "pet project", but if you're willing to contribute, contact admin