(function () {

    //document.addEventListener('DOMContentLoaded', init);

    var app = angular.module('main', ['xeditable']);

    app.run(function (editableOptions) {
        editableOptions.theme = 'bs3';
    });

    app.controller('CategoriesController', function ($scope, $timeout) {

        $scope.storageGetCallback = function ($items) {
            $scope.$apply(function () {
                $scope.categories = $items.categories;
                $scope.categories.sort(function (a, b) {
                    return a.name.localeCompare(b.name);
                });
                console.log('Pushed %s cats', $scope.categories.length);
                
                $scope.cloudLastDownload = $items.cloudLastDownload;
                $scope.cloudLastUpload = $items.cloudLastUpload;
            });
        };

        $scope.highlightRow = function (new_category, clazz) {
            $timeout(function () { new_category.class = clazz; }, 100).
                then(function () {
                    $timeout(function () { new_category.class = ''; }, 1500);
                });
        };

        $scope.categories = [];

        chrome.storage.local.get({categories: [], cloudLastDownload: null, cloudLastUpload: null}, $scope.storageGetCallback);

        $scope.removeCategoryRow = function (name) {
            var index = -1, catArr = $scope.categories;
            for (var i = 0; i < catArr.length; i++) {
                if( catArr[i].name === name ) {
                    index = i;
                    break;
                }
            }
            if( index === -1 ) {
                console.error( "Failed to remove category %s: couldn't find it in array", name );
            }
            $scope.categories.splice( index, 1 );
            chrome.storage.sync.set({categories:$scope.categories});
            console.log("Removed category match");
        };

        $scope.newcat = {hasError:false};

        $scope.saveCategories = function () {
            chrome.storage.local.set({categories:$scope.categories});
        };

        $scope.getCategoryByName = function (name) {
            for (var i=0; i < $scope.categories.length; i++) {
                if ($scope.categories[i].name === name) {
                    return $scope.categories[i];
                }
            }
            return null;
        };

        $scope.checkCategoryExists = function (catname) {
            var existingCat = $scope.getCategoryByName(catname);
            if (existingCat != null) {
                $scope.highlightRow(existingCat,'success');
                return true;
            } else {
                return false;
            }
        };

        $scope.addCategoryMatch = function () {
            if (!$scope.checkCategoryExists($scope.newcat.name)) {
                var new_category = $scope.newcat;
                $scope.categories.splice(0,0,$scope.newcat);
                $scope.saveCategories();
                console.log("Added new category match for %s", $scope.newcat.name);
                $scope.newcat = {hasError:false};
                $scope.highlightRow(new_category,'info');
            } else {
                console.error("Category already exists: %s", $scope.newcat.name);
                $scope.newcat.hasError = true;
                $scope.newcat.class='has-feedback has-error';
            }
        };
        
        $scope.exportCategoriesJson = function () {
            $scope.importedJson = JSON.stringify($scope.categories);
        }
        
        $scope.importCategoriesJson = function () {
            var jsonStr = $scope.importedJson;
            try {
                var json = JSON.parse(jsonStr);
                if (json.constructor === Array) {
                    console.log("Importing cats from Json.");
                    var cats = [];
                    for (var i=0; i<json.length; i++) {
                        var cat = json[i];
                        if (!$scope.isEmptyString(cat.name) && !$scope.isEmptyString(cat.match)) {
                            console.log("Importing %s = %s", cat.name, cat.match);
                            cats.push(cat);
                        }
                    }
                    $scope.categories = $scope.categories.concat(cats);
                
                    $scope.saveCategories();
                    
                    console.log("Done!");
                    
                    alert("Import done");
                    
                    $scope.importedJson = "";
                } else {
                    alert("Not a categories array");
                }
            } catch (e) {
                alert(e);
            }
        }
        
        $scope.importCategoriesXml = function () {
            var xmlStr = $scope.importedXml;
            var xmlDoc = ( new window.DOMParser() ).parseFromString(xmlStr, "text/xml");
            if (xmlDoc) {
                console.log("Importing categories from XML");
                try {
                    var catMatcher = xmlDoc.getElementById("categoryMatcher");
                    var map = catMatcher.getElementsByTagName("map")[0];
                    var elems = map.getElementsByTagName("entry");
                    var cats = [];
                    for (var i=0; i<elems.length; i++) {
                        var entry = elems[i];
                        var cat_name = entry.getAttribute("key");
                        var cat_matching = entry.getAttribute("value");
                        cat_matching = cat_matching.replace(/(\(\.\*)|(\.\*\))/g,"").replace(/\|/g,";");
                        console.log("Parsed %s = %s", cat_name, cat_matching);
                        cats.push({name: cat_name, match: cat_matching});
                    }
                    $scope.categories = $scope.categories.concat(cats);
                
                    $scope.saveCategories();
                    
                    console.log("Done!");
                    $scope.importedXml = '';
                } catch (err) {
                    console.error("Failed to import cats from xml:" + err.message);
                    console.log(xmlStr);
                }                
            } else {
                console.error("XML is incorrect");
                console.log(xmlStr);
            }
        };
        
        $scope.uploadToCloud = function () {
            console.log("Upload to cloud");
            chrome.storage.sync.set({categories:$scope.categories}, function() {
                $scope.updateLastUploadDate();
            });
        };
        
        $scope.downloadFromCloud = function () {
            console.log("Loading from cloud");
            chrome.storage.sync.get({categories: []}, function($items) {
                $items.cloudLastUpload = $scope.cloudLastUpload;
                $scope.storageGetCallback($items);
                $scope.saveCategories();
                $scope.updateLastDownloadDate();
            });
        };
        
        $scope.nowStr = function () {
            return new Date().toString('dddd, MMM d, yyyy HH:mm');
        };
        
        $scope.updateLastUploadDate = function () {
            var now = $scope.nowStr();
            $scope.$apply(function(){
                $scope.cloudLastUpload = now;
            });
            console.log("Uploaded to could on " + now);
            chrome.storage.local.set({cloudLastUpload:now});
        };
        
        $scope.updateLastDownloadDate = function () {
            var now = $scope.nowStr();
            $scope.$apply(function(){
                $scope.cloudLastDownload = now;
            });
            console.log("Downloaded from could on " + now);
            chrome.storage.local.set({cloudLastDownload:now});
        };
        
        $scope.isEmptyString = function (str) {
            return (!str || 0 === str.length);
        }

    });

})();