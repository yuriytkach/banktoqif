String.prototype.contains = function (it) {
    return this.indexOf(it) != -1;
};

var categories = [];

// Called when the user clicks on the browser action.
chrome.browserAction.onClicked.addListener(function (tab) {
    chrome.storage.local.get({
        categories: []
    }, function (items) {
        console.log("Got categories from local storage");
        categories = items.categories;
        processCategories(categories);
    });

    // No tabs or host permissions needed!
    console.log("Getting source through injection");
    chrome.tabs.executeScript(null, {
        file: "scripts/getPagesSource.js"
    });
});


chrome.extension.onMessage.addListener(function (request, sender) {
    if (request.action == "getSource") {
        console.log("Received source");
        var statements = parseOperations(request.source, request.frames);
        if (statements.error != null) {
            alert(statements.error);
            return;
        } else {

            convertStringsInStatements(statements);
            var qif = generateQIF(statements);

            //console.log(qif);
            chrome.tabs.create({
                url: "empty.html"
            }, function (tab) {
                console.log('Setting new body to ' + tab.id);
                setTimeout(function () {
                    chrome.tabs.sendMessage(tab.id, {
                        action: "getBody"
                        , source: qif
                    });
                }, 500);
            });

        }
    }
});

function processCategories(categories) {
    for (var i = 0; i < categories.length; i++) {
        var processed = [];
        var matches = categories[i].match.split(';');
        for (var j = 0; j < matches.length; j++) {
            var match = matches[j].replace('*', '.+').replace('(', '\(').replace(')', '\)');
            processed.push(match);
        }
        categories[i].matches = processed;
    }
}

function generateQIF(statements) {
    var rez = "!Type:Bank\n";
    for (var i = 0; i < statements.length; i++) {
        var st = statements[i];
        rez += "D" + st.date.toString("d/MM/yyyy") + "\n";
        rez += "T" + st.amountCard.amount + "\n";
        rez += "L" + st.category + "\n";
        rez += "M" + st.desc + "\n";
        rez += "Cc\n"; // Cleared status
        rez += "P\n";
        rez += "^\n";
    }
    return rez;
}

function removeWhitespace(str) {
    if (str != undefined) {
        return str.replace(/\s+/g, '');
    } else {
        return str;
    }
}

function removeAllDigitsDotsDashesColons(str) {
    return str.replace(/\d+/g, '').replace(/\./g, "").replace(/-/g, "").replace(/:/g, "");
}

function replaceNewLine(str) {
    return str.replace(/\n/g, " ");
}

function removeDotsAndReplaceComma(str) {
    return str.replace(/\./g, '').replace(/\,/g, '.');
}

var CURR_MAP_RU_EN = {
    ДОЛ: "USD"
    , ЕВРО: "EUR"
    , ГРН: "UAH"
    , евро: "EUR"
    , грн: "UAH"
    , долл: "USD"
    , дол: "USD"
};

var DATE_MONTH_MAP_RU_DIGIT = {
    ' января ': '/01/'
    , ' февраля ': '/02/'
    , ' марта ': '/03/'
    , ' апреля ': '/04/'
    , ' мая ': '/05/'
    , ' июня ': '/06/'
    , ' июля ': '/07/'
    , ' августа ': '/08/'
    , ' сентября ': '/09/'
    , ' октября ': '/10/'
    , ' ноября ': '/11/'
    , ' декабря ': '/12/'
};

function replaceFromMap(str, map) {
    for (var k in map) {
        str = str.replace(k, map[k]);
    }
    return str;
}

function parseAmount(amountStr) {
    if (amountStr != undefined) {
        var noSpace = removeWhitespace(amountStr);
        var num = parseFloat(noSpace);
        if (num != null) {
            var curr = removeAllDigitsDotsDashesColons(noSpace);
            curr = replaceFromMap(curr, CURR_MAP_RU_EN);
            return {
                amount: num
                , curr: curr
            };
        }
    }
    return undefined;
}

function processDescription(statement) {
    if (statement != undefined) {
        var desc = statement.desc;
        desc = replaceNewLine(desc);
        desc = desc.replace('205 - Безналичный платёж ', '').replace('205 - Безготівковий платіж\.', '');;
        if (statement.amountCurr != undefined && statement.amountCard.curr != statement.amountCurr.curr) {
            desc += " (" + statement.amountCurr.amount + " " + statement.amountCurr.curr;
            if (statement.rate != undefined) {
                desc += " @ " + statement.rate;
            }
            desc += ")";
        }
        desc += "."
        if (statement.fee != undefined || statement.cardName != undefined || statement.timeStr != undefined) {
            desc += " ["
            if (statement.fee != undefined && statement.fee.amount > 0) {
                desc += "Fee: " + statement.feeStr + ", ";
            }
            if (statement.cardName != undefined && statement.cardName != undefined) {
                desc += statement.cardName + ", ";
            }
            if (statement.timeStr != undefined && statement.timeStr != undefined) {
                desc += statement.timeStr;
            }
            desc += "]";
        }
        return desc;
    } else {
        return undefined;
    }
}

var lastDate = null;

function parseDate(dateStr) {
    dateStr = replaceFromMap(dateStr, DATE_MONTH_MAP_RU_DIGIT);

    dateStr = dateStr.replace("Сегодня", "today").replace("Вчера", "yesterday");
    var parsed;

    if (dateStr.indexOf("today") > -1 || dateStr.indexOf("yesterday") > -1) {
        dateStr = removeAllDigitsDotsDashesColons(dateStr);
    }

    parsed = Date.parse(dateStr);
    if (parsed == null) {
        console.error("Failed to parse date object from: " + dateStr);
        return new Date();
    }

    return parsed;
}

function matchCategory(desc) {
    for (var i = 0; i < categories.length; i++) {
        var matches = categories[i].matches;
        for (var j = 0; j < matches.length; j++) {
            if (desc.match(matches[j]) != null) {
                return categories[i].name;
            }
        }
    }
    return "Other";
}

function convertStringsInStatements(statements) {
    Date.CultureInfo.dateElementOrder = "dmy";
    statements.forEach(function (st) {
        st.amountCard = parseAmount(st.amountCardStr);
        if (st.amountCurrStr != undefined) {
            st.amountCurr = parseAmount(st.amountCurrStr);
        }
        if (st.rate == undefined && st.amountCurr != undefined) {
            if (Math.abs(st.amountCard.amount) > Math.abs(st.amountCurr.amount)) {
                st.rate = Math.abs(st.amountCard.amount / st.amountCurr.amount).toFixed(2) + ' ' + st.amountCurr.curr + st.amountCard.curr;
            } else {
                st.rate = Math.abs(st.amountCurr.amount / st.amountCard.amount).toFixed(2) + ' ' + st.amountCard.curr + st.amountCurr.curr;
            }
        }
        st.fee = parseAmount(st.feeStr);
        st.date = parseDate(st.dateStr);
        st.category = matchCategory(st.desc);
        st.desc = processDescription(st);
    });
}

function parseOperations(source, frames) {
    var doc = new DOMParser().parseFromString(source, "text/html");

    var panelGroup = doc.getElementsByClassName('panel-group');
    if (panelGroup.length == 0) {
        if (frames.length > 0) {
            for (var i = 0; i < frames.length; i++) {
                var frame = frames[i];
                doc = new DOMParser().parseFromString(frame, "text/html");
                panelGroup = doc.getElementsByClassName('panel-group');
                if (panelGroup.length != 0) {
                    break;
                }
            }
        }
    }
    var result = [];

    if (panelGroup.length == 0) {
        var error = "Not a valid Privat24 report.";
        console.debug(error);
        result = parseAvalReport(doc);
        if (result == null) {
            var error2 = "Not a valid Aval report.";
            console.error(error2);
            return {
                error: error + '\n\n' + error2
            };
        }
    } else {
        console.log('Panel-Group found. Parsing divs: ' + panelGroup[0].children.length);

        for (var i = 0; i < panelGroup[0].children.length; i++) {
            var stm = panelGroup[0].children[i];
            if (stm.tagName == 'DIV') {
                var date = stm.getElementsByClassName('b_txt')[0].innerText;

                var div = stm.getElementsByClassName('t_statement')[0];

                var desc = div.getElementsByClassName('t_descr')[0].innerText.trim();
                var timeStr = div.getElementsByClassName('t_time')[0].innerText.trim();
                var amountCard = parseCardAmount(div.getElementsByClassName('t_amt')[0]);
                var amountCurr = parseCurrAmountAndDetails(stm.getElementsByClassName('details')[0]);
                var statement = {};
                statement.timeStr = timeStr;
                statement.dateStr = date;
                statement.desc = desc;
                statement.amountCardStr = amountCard.amount + ' ' + amountCard.curr;
                if (amountCurr.amountWithCurr != undefined) {
                    statement.amountCurrStr = amountCurr.amountWithCurr;
                    statement.rate = amountCurr.rate;
                }
                statement.feeStr = amountCard.fee + ' UAH';
                statement.cardName = amountCurr.cardName;
                result.push(statement);
            }
        }
    }

    return result;
}

function parseAvalReport(doc) {
    var tables = doc.getElementsByTagName('table');
    if (tables.length == 0) {
        console.warn('No tables found');
        return null;
    } else {
        var avalOnlineCard = false;
        var avalOnlineDepo = false;

        var body = doc.getElementsByTagName('body')[0];

        if (body.textContent.indexOf('Історія по картковому рахунку') !== -1) {
            avalOnlineCard = true;
        } else if (body.textContent.indexOf('Історія по депозитному рахунку') !== -1) {
            avalOnlineDepo = true;
        }

        if (avalOnlineCard) {
            return parseAvalOnlineCardReport(doc.getElementById('__bookmark_12'));
        } else if (avalOnlineDepo) {
            return parseAvalOnlineDepoReport(doc.getElementById('__bookmark_13'));
        } else {
            return parseAvalVipiskaReport(tables);
        }
    }
    return null;
}

function parseAvalOnlineCardReport(table) {
    if (table == null) {
        console.error('Table for parsing aval online card records is not found');
        return null;
    } else {
        console.info('Parsing AvalOnline Card statements');
        var rows = table.rows;
        result = [];
        for (var i = 1; i < rows.length; i++) {
            var cells = rows[i].children;

            if (cells[0].innerText.trim() != '') {
                var date = cells[0].innerText.trim().split('\n')[0].trim();
                var desc = cells[3].innerText.trim();
                var amountCard = cells[5].innerText.trim();
                var amountCurr = cells[6].innerText.trim();
                var curr = cells[7].innerText.trim();

                var statement = {};
                statement.dateStr = date;
                statement.desc = desc;
                statement.amountCardStr = removeDotsAndReplaceComma(amountCard) + ' ' + 'UAH';
                if (amountCard != amountCurr) {
                    statement.amountCurrStr = removeDotsAndReplaceComma(amountCurr) + ' ' + curr;
                }

                console.debug(statement);
                result.push(statement);
            }
        }
        return result;
    }
}

function parseAvalOnlineDepoReport(table) {
    if (table == null) {
        console.error('Table for parsing aval online deposit records is not found');
        return null;
    } else {
        console.info('Parsing AvalOnline Deposit statements');
        var rows = table.rows;
        result = [];
        for (var i = 1; i < rows.length; i++) {
            var cells = rows[i].children;

            if (cells[0].innerText.trim() != '') {
                var date = cells[0].innerText.trim().split('\n')[0].trim();
                var desc = cells[1].innerText.trim();
                var amountCard = cells[2].innerText.trim();
                var curr = cells[3].innerText.trim();

                var statement = {};
                statement.dateStr = date;
                statement.desc = desc;
                statement.amountCardStr = removeDotsAndReplaceComma(amountCard) + ' ' + curr;

                console.debug(statement);
                result.push(statement);
            }
        }
        return result;
    }
}

function parseAvalVipiskaReport(tables) {
    for (var i = 0; i < tables.length; i++) {
        if (tables[i].getAttribute('colspan') == 9) {
            var rows = tables[i].children[0].children;
            result = [];
            for (var j = 0; j < rows.length; j++) {
                if (rows[j].getAttribute('type') == null && rows[j].children.length > 6) {
                    var cells = rows[j].children;

                    var date = cells[1].innerText.trim();
                    var desc = cells[3].innerText.trim();

                    var amountPlus = cells[4].innerText.trim();
                    var amountMinus = cells[5].innerText.trim();

                    var amountCard;
                    if (amountMinus == undefined || amountMinus == '') {
                        amountCard = amountPlus;
                    } else {
                        amountCard = '-' + amountMinus;
                    }

                    var amountCurr = cells[8].innerText.trim();
                    var curr = cells[7].innerText.trim();

                    var statement = {};
                    statement.dateStr = date;
                    statement.desc = desc;
                    statement.amountCardStr = amountCard + ' ' + 'UAH';
                    if (amountCard != amountCurr) {
                        statement.amountCurrStr = amountCurr + ' ' + curr;
                    }

                    console.debug(statement);
                    result.push(statement);
                }
            }
            return result;
        }
    }
}

function parseCardAmount(div) {
    var rez = {};
    var spans = div.getElementsByTagName('span');
    for (var i = 0; i < spans.length; i++) {
        var span = spans[i];
        if (span.getAttribute('bold-money') == 'transaction.amount') {
            rez.amount = span.innerText;
        } else if (span.getAttribute('set-text') == 'transaction.amountCurrency') {
            rez.curr = span.innerText;
        } else if (span.getAttribute('bold-money') == 'transaction.fee') {
            rez.fee = span.innerText;
        }
    }
    return rez;
}

function parseCurrAmountAndDetails(div) {
    var rez = {};
    var spans = div.getElementsByTagName('span');
    for (var i = 0; i < spans.length; i++) {
        var span = spans[i];
        if (span.getAttribute('set-text') == 'util.formatCardName(transaction)') {
            rez.cardName = span.innerText;
            break;
        }
    }
    var divs = div.getElementsByTagName('div');
    for (var i = 0; i < divs.length; i++) {
        div = divs[i];
        if (div.getAttribute('original-amount') == 'transaction') {
            full_txt = div.innerText;
            amount_curr = full_txt.split(':')[1];
            arr = amount_curr.split(" ");
            rez.amountWithCurr = arr[1];
            break;
        }
    }
    return rez;
}